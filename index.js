// Llamadas a módulos
const environment = require('./environment'); // Configuración del entorno de ejecución
const authentication = require('./env/auth.json'); // Configuración del Usuario y Contraseña para uso del servicio.
const express = require('express'); // Módulo express para creacción del servidor
const jwt = require('jsonwebtoken'); // Módulo express para autenticación
const moment = require('moment'); // Módulo moment para trabajar con momentos, pasarlos a fechas y viceversa
const bodyParser = require('body-parser'); // Módulo express para analiza los data de la publicación y los agregarlos a req.body .

console.log(environment);
console.log(authentication);

// Constante que el contiene servidor express
const app = express();

// Indicamos la configuración de nuestra clave ultra secreta
app.set('key', authentication.key );

// seteamos para que el body-parser nos convierta lo que viene del cliente
app.use(bodyParser.urlencoded({ 
    extended: true 
}));

// La API trabaja en formato JSON
app.use(bodyParser.json());

// Arrancamos el servidor en el puerto 3000
app.listen(environment.port, () => {
    console.log('API iniciada en el puerto 3000') 
});

// Generamos un “punto de inicio”
app.get('/', (req, res) => {
    const data = {
        message: "Página de inicio", 
        status: true
    }
    res.status(200).json(data);
});


/*
    Autenticación del cliente, envía usr y psw, se autentica y se le envía su token desesión con una duración de 24 horas.
*/

const createToken = () => {
    // Payload a pasar a jwt para la creacción del token, check ha de ser true indicando que la autenticación es correcta.
    const payload = {
        check:  true,
        iat: moment().unix(), //Mometo en el que se creo el token
        exp: moment().add(authentication.exp, authentication.expUnits).unix() // Momento en que expirará el token
    };
    // Creacción y configuración del token a partir de la llave "key" del app (app.get('key)) de forma asíncrona.
    return jwt.sign(payload, app.get('key'));
}


app.post('/auth', (req, res) => {
    /*
        Extraemos de las cabeceras la Basic Authentication (usuario y contraseña), para ello hemos de conocer su aspecto en cabecera:
            {
                "authorization": "Basic Y3ZjZHZjOnNkZnNkZnNk"
            }
        La clave y usuario encriptados: Y3ZjZHZjOnNkZnNkZnNk
        Tipo de encriptado: Basic
        
        Para el caso Basic, separamos con split en un array de 2 elementos el string en authorization que se encuentra en la cabecera
        de la petición (request): base64Credentials, authenticationType
    */
    const authenticationType =  req.headers.authorization.split(' ')[0];
    var  [username, password] = [undefined,undefined];

    // Si la autenticación es del tipo "Basic"...
    if(authenticationType === 'Basic'){
        // Extraigo la autenticación encriptada
        const base64Credentials =  req.headers.authorization.split(' ')[1];
        // Desencript pasando a ascii
        const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
        //separo usuario y contraseña para comparar más adelante: username, password
        [username, password] = credentials.split(':');
    }

    // Si el usr y la psw son correctas...
    if(username === authentication.basicAuth.user && password === authentication.basicAuth.password) {
        
        // Creacción síncrona del token
        let token = createToken();

        res.status(201).json({
            message: 'AuthorizedAccess',
            token: token,
            status: true,
        });

    } else {
        // Si el usr y la psw no son correctas...
        res.status(401).json({ 
            name: "AuthenticationFailed",
            message: "Access denied", 
            status: false
        });
    }
});

/*
    Capa de midlewearwe (comprueba que el token se encuentre en las cabeceras y que sea correcto).

    express.Router() ==> Ha de contener las rutas que van a ser protegidas por ésta capa. El acceso 
    a dichas rutas será rechazado si el cliente no ha añadido un token válido a la cabecera de su 
    petición o si ha caducado.
*/
 
const protectedRoutes = express.Router(); 

protectedRoutes.use((req, res, next) => {
    /*
        Accedemos a las cabeceras de la petición del cliente (requeste ~ req) en búsqueda del token
        que se le asignó previamente. Retorna undefined si no existe el campo access-token en las
        cabeceras.
    */
    const token = req.headers['access-token'];    

    // Si existe el token en las cabeceras...
    if (token != undefined) {
        // Se inicia el proceso de autenticación del token, usando la clave maestra "key" del servidor express de forma asíncrona
        jwt.verify(token, app.get('key'), (err, decoded) => { 
            // Si la autenticación del token falla (err === true)
            if (err) {
                console.log(err);
                return res.status(401).json(err);   

            } else {// Si la autenticación del token es correcta (err === false)
                req.decoded = decoded;
                res.data = {};

                let current_time = new Date().getTime() / 1000; // Momento actual
                let limit_time = new Date(req.decoded.exp * 1000); // Momento límite 
                limit_time.setMinutes(limit_time.getMinutes()-authentication.updateTokenWindowTime);
                limit_time = limit_time.getTime() / 1000;
                
                // Si aún no ha expirado el token y aún estamos dentro del límite aceptable, damos un nuevo token
                if (limit_time < current_time) { 
                    // Creacción síncrona del token
                    const newToken = createToken();
                    req.decoded = jwt.verify(newToken, app.get('key'));
                    const newTokenObject={
                        message: 'NewTokenUpdate',
                        token: newToken,
                        status: true,
                    };
                    // Si se ha actualizado el token, lo añado
                    res.data = Object.assign(res.data, newTokenObject);
                    console.log(newTokenObject);
                }
                // Continúa el proceso de enrutamiento saltando a la siguiente función del proceso
                next();
                
            }
        });

    } else { // Si no se ha dado un token en la cabecera...
      res.status(401).json({ 
          name: 'TokenNotProvided',
          message: 'Access denied, token not inserted in header',
          status: false
      });
    }
 });

// Ruta que requiere de autenticación por token en su cabecera
app.get('/data', protectedRoutes, (req, res) => {
    // Si es que se actualiza, el token nuevo está en el objeto res.data...
    res.data.data = [
                { id: 1, name: "Miguel" },  
                { id: 2, name: "Miren" },
                { id: 3, name: "Amine" }
            ];

    res.status(200).json(res.data);
});