/*
    ====================================================================================================================
    Autor: Miguel San Bernardino Martín
    Fecha: 14 - nov - 2019
    Descripción: Cargando las propiedades del entorno desde un "archivo de propiedades" especificado como argumento en
        línea de comandos con env=<nombreDelArchivo>
    ====================================================================================================================
*/
process.argv.forEach((val, index, array) => {
    var arg = val.split("=");
    if (arg.length > 0) { 
        if (arg[0] === 'env') { 
            try{
                var env = require('./env/' + arg[1] + '.json');
                module.exports = env;
            }catch(err){
                console.error('No se encuentra el archivo de propiedades pasado por argumento: \n ' + err);
                process.exit(1);
            }
        }
    }
});