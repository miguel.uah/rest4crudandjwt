# Introducción 

**Autor**: Miguel San Bernardino Martín

**Última actualización**: 18 - nov - 2019

**Descripción**: Aplicación REST para DRUD con JsonWebToken. 

# Funcionamiento:
![GitHub Logo](/img/diagramaDeFuncionamineto.jpeg)

* 0º Inicia servidor en puerto configurado en environment.

**Autenticación**

* 1º Acceso POST a /auth con autenticación básica de usuario y contraseña configurados en auth.json
* 2º Si hay exito genera un token a partir de la clave maestra y duración configuradas en auth.json y lo retorna.
* 3º Acceso POST,PUT,GET, DELETE usando en Token administrado insertando en la cabecera de la petición en un campo
llamado *access-token*.

**Midleweare**

* 4º Si hay token => ¿Ha caducado?
* 5º Si no ha caducado ¿está dentro de la ventana de actualización de token? tamaño de la ventana en minutos en 
auth.json
* 6º Retorna contenido y nuevo token si aplica al cliente.
* 7º Si se retorna un error 401 ha de regresar al paso 1.

# Sugerencias:

* Puede incrementarse la seguridad empleando clabes RSA para encriptar en lugar de la existente en auth.json
* Puede incrementarse la seguridad añadiendo un servidor https junto al servidor express.

# Instalar:

* **Desde el directorio raiz del proyecto**: npm install

# Ejecutar:

* **Configuración de desarrollador**: sudo npm start env=dev

* **Configuración de producción**: sudo npm start env=qa